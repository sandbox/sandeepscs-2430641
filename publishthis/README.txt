README for Publishthis for Drupal 7
-----------------------------------------------------------

Maintainers:
 * InfoBeans (India, Pune)
   sandeep.scs@infobeans.com, 
   mangesh.shinde@infobeans.com

Project homepage: https://www.drupal.org/sandbox/sandeepscs/2430641

INTRODUCTION
------------

It provides options to import Publishthis content as node on your Drupal site,
and show content as a Block.

INSTALLATION
-------------------------
1. Enable the module at module administration page
   -- Publish This content type will automatically 
      create on your system after installing it.
      
CONFIGURATION
-------------

1. Publishthis api configuration
   -- Click on "configure" to do the 
      configuration settings.
2. Fill in required settings i.e 'Publishthis api url' and 'Publishthis key'.
   -- "Publishthis key" is the key that is 
       on Publish This server.
   -- log in to the manager section of publishthis. 
      (https://manager.publishthis.com/admin)  
      API Token Value will be the 'Publishthis key'.
3. Publishthis block setting   
   -- Do the setting for number of publish this blocks to show.

   
BUILT-IN FEATURES
=================
This module automatically creates the "PublishThis content type" 
on your database after installing it.
To check, visit "Home » Administration » Structure" 
after you install Publishthis.
   
   
NOTES : 
-------------
Publishthis curated content:- 'Published feeds' will 
show the list of imported published feeds from Publishthis in dropdown.
You just need to select the feed from dropdown and click to get content. 
It will show the all the curated contents from the selected feed.
You can select the contents and click to save content, 
it will automatically saved into your Drupal database.

Publishthis feed content:- It will show the automated 
contents from publishthis end. 
You just need to follow above procedure.

Publishthis block setting:- Number of blocks will get 
created as per setting done by you. Configure the blocks 
to show the publishthis content as block.
