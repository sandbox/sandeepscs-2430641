<?php

/**
 * @file
 * Configuration form setting written here.
 */

/**
 * Publishthis admin settings.
 */
function publishthis_admin_settings() {
  $form = array();
  drupal_set_title(t("Publishthis api configuration"));

  $form['publishthis_api_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('PublishThis api configuration'),
    '#collapsible' => TRUE,
    '#prefix' => '<div id="fieldset-external">',
    '#suffix' => '</div>',
  );
  $form['publishthis_api_config']['publishthis_server'] = array(
    '#title' => t('Publishthis api url'),
    '#type' => 'textfield',
    '#default_value' => variable_get('publishthis_server', ' '),
    '#description' => t('Enter api server url with http:// e.g. http://webapi.publishthis.com etc.'),
  );
  $form['publishthis_api_config']['publishthis_key'] = array(
    '#title' => t('Publishthis key'),
    '#type' => 'textfield',
    '#default_value' => variable_get('publishthis_key', ''),
    '#description' => t('Enter publishthis api token key'),
  );

  $form['publishthis_block_setting'] = array(
    '#type' => 'fieldset',
    '#title' => t('PublishThis Block Setting'),
    '#collapsible' => TRUE,
    '#prefix' => '<div id="fieldset-external">',
    '#suffix' => '</div>',
  );

  $form['publishthis_block_setting']['link'] = array(
    '#markup' => '<a href="/admin/config/development/publishthis" target="_blank">Publishthis block setting</a>',
    '#prefix' => '<div style="margin-top: 10px;">',
    '#suffix' => '</div>',
  );

  $form['publishthis_get_curate_content'] = array(
    '#type' => 'fieldset',
    '#title' => t('PublishThis Get Curated content'),
    '#collapsible' => TRUE,
    '#prefix' => '<div id="fieldset-external">',
    '#suffix' => '</div>',
  );

  $form['publishthis_get_curate_content']['link'] = array(
    '#markup' => '<a href="/admin/config/development/curatedbundlecontent" target="_blank">Publishthis curated content</a>',
    '#prefix' => '<div style="margin-top: 10px;">',
    '#suffix' => '</div>',
  );

  $form['publishthis_get_automate_content'] = array(
    '#type' => 'fieldset',
    '#title' => t('PublishThis Get Automate content'),
    '#collapsible' => TRUE,
    '#prefix' => '<div id="fieldset-external">',
    '#suffix' => '</div>',
  );

  $form['publishthis_get_automate_content']['link'] = array(
    '#markup' => '<a href="/admin/config/development/bundleautocontent" target="_blank">Publishthis automated (feed) content</a>',
    '#prefix' => '<div style="margin-top: 10px;">',
    '#suffix' => '</div>',
  );
  return system_settings_form($form);
}
