<?php
/**
 * @file
 * Implemented common curl function to fetch the data from publish this.
 */

/**
 * Get publishthis data.
 */
function get_publish_thisdata($url) {
  // Init the json curl.
  $ch = curl_init();
  // Set to zero for no timeout.
  $timeout = 5;
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
  $file_contents = curl_exec($ch);
  curl_close($ch);
  $returndata = json_decode($file_contents);
  if (is_null($returndata)) {
    // If version of php is >= 5.3.0 then use json_last_error function.
    if (strnatcmp(phpversion(), '5.3.0') >= 0) {
      switch (json_last_error()) {
        case JSON_ERROR_NONE:
          $returndata->error = 'json_decode: No errors';
          break;

        case JSON_ERROR_DEPTH:
          $returndata->error = 'json_decode: Maximum stack depth exceeded';
          break;

        case JSON_ERROR_STATE_MISMATCH:
          $returndata->error = 'json_decode:  Underflow or the modes mismatch';
          break;

        case JSON_ERROR_CTRL_CHAR:
          $returndata->error = 'json_decode:  Unexpected control character found';
          break;

        case JSON_ERROR_SYNTAX:
          $returndata->error = 'json_decode:  Syntax error, malformed JSON';
          break;

        case JSON_ERROR_UTF8:
          $returndata->error = 'json_decode:  Malformed UTF-8 characters, possibly incorrectly encoded';
          break;

        default:
          $returndata->error = 'Error: Could not get data';
          break;
      }
    }
    else {
      $returndata->error = "json_decode did not return a valid data stream.";
    }
  }
  return $returndata;
}

/**
 * Implement function to get the feed list from publish this.
 */
function get_bundle_contents() {
  // Prepare api url with token details.
  $url = PUBLISHTHIS_BUNDLE_API . "?token=" . TOKEN . "&results=20";
  // Get data by calling the webservice.
  $response_array = get_publish_thisdata($url);
  // Format the respone data as per required format.
  $response = get_formatted_data($response_array);
  $exist_category = FALSE;

  if (!empty($response)) {
    if (!is_object($response)) {
      $form['markup'] = array(
        '#type' => 'markup',
        '#value' => '<div class="messages error">' . $response . '</div>',
      );
    }
    else {
      $response_array = $response;
      $saved_array = $response_array->resultList;
      $selectitems[0] = '--Select published feed--';
      foreach ($saved_array as $data) {
        $exist_category = TRUE;
        $selectitems[$data->feedId] = $data->title;
      }
      natcasesort($selectitems);
    }
  }
  // Get feedid and page no from url using arg() function.
  $page = 0;
  $arguments = arg();
  $arg_len = count($arguments);
  $pg_no = $arguments[$arg_len - 1];
  $select_id = $arguments[$arg_len - 2];
  if (!is_numeric($select_id)) {
    $select_id = 0;
  }
  if (is_numeric($pg_no)) {
    $page = $pg_no;
  }

  // Prepare a form to display feed in dropdown with submit button.
  // To get their content.
  drupal_set_title(t("Publishthis curated content"));

  if ($exist_category == TRUE) {
    $form['#attributes'] = array('name' => 'publishform');

    $form['savedid'] = array(
      '#type' => 'select',
      '#options' => $selectitems,
      '#value' => $select_id,
      '#prefix' => '<br/><fieldset><legend style="margin-left: 10px;">Published feeds</legend><div class="description" style="margin-left: 10px;">List of imported published feeds from Publishthis.</div><div id="edit-operation-wrapper" style="margin-left: 10px;" class="form-item">',
    );

    $form['savedid_hidden'] = array(
      '#type' => 'hidden',
    );

    $form['page_no'] = array(
      '#type' => 'hidden',
      '#value' => '0',
    );

    $form['button23'] = array(
      '#markup' => "<input type='button' class='form-submit' onclick='get_contents(0,\"GetBundleContents\");' name='button' value='Get content' style='height: 27px;margin-top: 10px;' title='Import contents from Publishthis for selected criteria' />",
      '#suffix' => '</div></fieldset>',
    );

    $module_path = drupal_get_path('module', 'publishthis');

    $image_path = "/" . $module_path . "/images/loading.gif";

    $form['submit'] = array(
      '#attributes' => array("style" => "height:27px; margin-bottom:2px; margin-top:10px;", 'title' => 'Click to save selected content'),
      '#type' => 'submit',
      '#value' => 'Save content',
      '#prefix' => '<div id="update_div" style=""><fieldset><legend style="margin-left: 10px;">Import content</legend><div class="description" id="submit_div" style="margin-left: 10px;">To import content please select checkbox against content and click Save content button.<br/>',
      '#suffix' => '</div><div id="response" style="margin-left: 10px;"></div></fieldset></div>',
    );

    $form['jssubmit'] = array('#markup' => '<script> var loading_img = "' . $image_path . '"; get_contents(' . $page . ',\'GetBundleContents\'); </script>');
  }
  return $form;
}

/**
 * Return result/contents of bundle feed in json format using Ajax.
 */
function _get_bundle_contents($ids, $page = '', $con_type = '') {
  // Get existing published id from our database.
  $existing_pubids = get_existing_pubids();
  $module_path = drupal_get_path('module', 'publishthis');
  $correct_img = "/" . $module_path . "/images/correct.gif";
  $del_img = "/" . $module_path . "/images/del.png";

  if (!empty($ids)) {
    // Pagination setting initialization.
    $per_page = RECORDS_PER_PAGE;
    if (empty($page)) {
      $page = 0;
    }
    $num_pages = 0;
    $skip = $page * $per_page;
    if (is_array($ids)) {
      $ids = implode(",", $ids);
    }
    if ($con_type == 'autocontent') {
      // Prepare api url with token, feed id and skip parameter.
      $url = PUBLISHTHIS_BUNDLE_AUTO_CONTENT_API . $ids . "?token=" . TOKEN . "&skip=" . $skip . '&results=' . $per_page;
      $next_url = PUBLISHTHIS_BUNDLE_AUTO_CONTENT_API . $ids . "?token=" . TOKEN . "&skip=" . ($skip + $per_page) . '&results=' . $per_page;
    }
    else {
      // Prepare api url with token, feed id and skip parameter.
      $url = PUBLISHTHIS_BUNDLE_CURATED_CONTENT_API . $ids . "?token=" . TOKEN . "&skip=" . $skip . '&results=' . $per_page;
      $next_url = PUBLISHTHIS_BUNDLE_CURATED_CONTENT_API . $ids . "?token=" . TOKEN . "&skip=" . ($skip + $per_page) . '&results=' . $per_page;
    }

    // Get data by calling the webservice.
    $response_array = get_publish_thisdata($url);

    // Format the respone data as per required format.
    $response = get_formatted_data($response_array);

    if (!empty($response)) {
      if (!is_object($response)) {
        $news_content = '<table >';
        $news_content .= "<tr><td colspan=3><div class=\"messages error\">" . $response . "</div></td></tr></table>";
        $news_content .= '<script>
	       jQuery("#submit_div").hide();
	      </script>';
      }
      else {
        $result_list = $response;
        $saved_array = $result_list->resultList;
        // Prepare the html result list of return webservice data.
        if (!empty($result_list->resultList)) {
          $news_content = '<table >';
          $news_content .= "%err-msg%";
          $news_content .= "<tr><th>%checkbox%</th><th><strong>Image</strong></th><th><strong>Title</strong></th><th><strong>Summary</strong></th></tr>";
          $k = 0;
          $imported_status = FALSE;
          foreach ($saved_array as $data) {
            $class = 'even';
            if ($k % 2 == 0) {
              $class = 'odd';
            }

            if (!empty($data->imageUrl)) {
              $img = "<img src='" . $data->imageUrl . "' width=50 height=50 />";
            }
            else {
              $img = "&nbsp;";
            }

            $summary_data = strip_tags($data->summary);
            if (count($summary_data) > 100) {
              $summary_data .= substr($summary_data, 0, 100) . " ...";
            }

            $title = $data->title;
            if (!empty($data->url)) {
              $title = "<a href='" . $data->url . "' target='_blank' >" . $title . "</a>";
              $summary_data .= " <a href='" . $data->url . "' target='_blank' >Read more >></a>";
            }

            if (in_array($data->docId, $existing_pubids)) {
              $news_content .= "<tr valign=top class='" . $class . "'><td  style='width: 60px;'><img src='" . $correct_img . "'  title='Content already imported'  style='margin-right: 5px;' /><a href='javascript:void(0);' onclick='delete_node(\"/" . PUBLISHTHIS_DELETE_URL . "/" . $data->docId . "\");' ><img src='" . $del_img . "' title='Delete imported content' /></a></td><td>" . $img . "</td><td>" . $title . "</td><td>" . $summary_data . "</td></tr>";
            }
            else {
              $imported_status = TRUE;
              $news_content .= "<tr valign=top class='" . $class . "'><td><input type='checkbox'  class='case' name='content_ids[]' value='" . $data->docId . "' title='Click to select/deselect content' /></td><td>" . $img . "</td><td>" . $title . "</td><td>" . $summary_data . "</td></tr>";
            }
            $k++;
          }
          $news_content .= '<tr><td>%checkbox%</td><td colspan=3><div  class="item-list">';

          // Get pagination html by calling the pagination function.
          $config = array();
          $config['num_pages'] = $num_pages;
          $config['page'] = $page;
          if ($con_type == 'autocontent') {
            $config['method'] = 'GetBundleAutoContents';
          }
          else {
            $config['method'] = 'GetBundleContents';
          }

          // Added api call to check next page contents avaialbe or not.
          $response_array = get_publish_thisdata($next_url);
          $config['next_flag'] = FALSE;
          if (!empty($response_array->resp->data->resultList)) {
            $config['next_flag'] = TRUE;
          }

          $news_content .= get_publishthis_pagination($config);
          $news_content .= '</div></td></tr>';
          $news_content .= '<tr><td colspan=4><div id="submit_div2"><input type="submit" class="form-submit" value="Save content" id="edit-submit" name="op" title="Click to save selected content" style=\'height:27px;margin-left:-7px;\' ></div></td></tr>';
          if ($imported_status == TRUE) {
            $search_arr = array("%checkbox%", "%err-msg%");
            $replace_arr = array("<input type='checkbox' id='selectall' class='selectall'  title='Click to select/deselect all content' />", "");
            $news_content = str_replace($search_arr, $replace_arr, $news_content);
            $news_content .= '<script>jQuery("#submit_div").show();jQuery("#submit_div2").show();</script>';
          }
          else {
            $search_arr = array("%checkbox%", "%err-msg%");
            $replace_arr = array("&nbsp", "<tr><td colspan=4><div class=\"messages error\">All content from this page are already imported. To import more content please navigate other pages.</div></td></tr>");
            $news_content = str_replace($search_arr, $replace_arr, $news_content);
            $news_content .= '<script>jQuery("#submit_div").hide();jQuery("#submit_div2").hide();</script>';
          }
        }
      }
    }
    else {
      $news_content = '<table >';
      $news_content .= "<tr><td colspan=3>Unable to retrieve content for specified criteria.</td></tr></table>";
      $news_content .= '<script>
       jQuery("#submit_div").hide();
      </script>';
    }
  }
  else {
    $news_content = '<table >';
    $news_content .= "<tr><td colspan=3>Select published feed from drop down list and click on \"Get content\" to fetch data from Publishthis.</td></tr></table>";
    $news_content .= '<script>
	       jQuery("#submit_div").hide();
	      </script>';
  }
  // Parse not printable characters like ascii-nonascii etc.
  $return_news_content = preg_replace('/[^(\x20-\x7F)]*/', '', $news_content);

  return $return_news_content;
}


/**
 * Return result/contents of bundle feed in json format using Ajax.
 */
function _get_bundle_contents_listing($ids, $page = '', $con_type = '') {
  if (empty($_REQUEST['page'])) {
    $page = '';
  }
  else {
    $page = $_REQUEST['page'];
  }

  if (!empty($ids)) {
    // Pagination setting initialization.
    $per_page = RECORDS_PER_PAGE;
    if (empty($page)) {
      $page = 0;
    }

    $skip = $page * $per_page;
    if (is_array($ids)) {
      $ids = implode(",", $ids);
    }
    if ($con_type == 'autocontent') {
      // Prepare api url with token, feed id and skip parameter.
      $url = PUBLISHTHIS_BUNDLE_AUTO_CONTENT_API . $ids . "?token=" . TOKEN . "&skip=" . $skip . '&results=' . $per_page;
    }
    else {
      // Prepare api url with token, feed id and skip parameter.
      $url = PUBLISHTHIS_BUNDLE_CURATED_CONTENT_API . $ids . "?token=" . TOKEN . "&skip=" . $skip . '&results=' . $per_page;
    }

    // Get data by calling the webservice.
    $response_array = get_publish_thisdata($url);
    // Format the respone data as per required format.
    $response = get_formatted_data($response_array);

    if (!empty($response)) {
      if (!is_object($response)) {
        $news_content = '<table >';
        $news_content .= "<tr><td colspan=3><div class=\"messages error\">" . $response . "</div></td></tr></table>";
        $news_content .= '<script>
	       jQuery("#submit_div").hide();
	      </script>';
      }
      else {
        $result_list = $response;
        $saved_array = $result_list->resultList;
        // Prepare the html result list of return webservice data.
        if (!empty($result_list->resultList)) {
          $result = '<div id="tabcontainer"><div class="tab-header">
						<h3>Night Club - Publishthis automated</h3><br>
					  </div><div class="auto_listing">';
          foreach ($saved_array as $data) {
            $pubishdate = $data->publishDate;
            $pubishdate = substr($pubishdate, 0, 10);
            $pubishdate = date("F d,Y", strtotime($pubishdate));
            $title = "";
            $summary_data = "";
            $summary = "";
            if (!empty($data->imageUrl)) {
              $img = "<img width='100' src='" . $data->imageUrl . '?W=100&H=67' . "'/>";
            }
            else {
              $img = "&nbsp;";
            }

            if (!empty($data->url)) {
              $title = "<a href='" . $data->url . "' target='_blank' >" . $data->title . "</a>";
              $summary_data .= " <span class='pub_more'><a href='" . $data->url . "' target='_blank' >More</a>>></span>";
            }
            if (!empty($data->summary)) {
              $summary = "<p>" . $data->summary . "$summary_data</p>";
            }

            $result .= "<div class='pub_automated_content'>";
            $result .= "<div class='pub_auto_image'>$img</div>";
            $result .= "<div class='pub_auto_title'>$title <div class='pub_date'> $pubishdate</div></div>";
            $result .= "<div class='pub_auto_summary'>$summary</div>";
            $result .= '</div>';
          }

          $result .= '</div>';
          $result .= '</div>';
          $result .= get_pagination_listing($page);
        }
      }
    }
    else {
      $result = '<table >';
      $result .= "<tr><td colspan=3>Unable to retrieve content for specified criteria.</td></tr></table>";
    }
  }
  // Parse not printable characters like ascii-nonascii etc.
  $return_news_content = preg_replace('/[^(\x20-\x7F)]*/', '', $result);
  return $return_news_content;
}

/**
 * Implements hook_submit() for get_bundle_contents().
 */
function get_bundle_contents_submit(&$form_state, $form) {
  // Extract all post parameter.
  $content_ids = $_POST['content_ids'];
  $savedid = $_POST['savedid'];
  $page_no = $_POST['page_no'];
  extract($_POST);
  $skip = 0;
  if (!empty($content_ids) && !empty($savedid)) {
    if ($page_no > 0) {
      $skip = $page_no * RECORDS_PER_PAGE;
    }
    // Prepare the api url to fetch the data from publish this.
    $url = PUBLISHTHIS_BUNDLE_CURATED_CONTENT_API . $savedid . "?token=" . TOKEN . "&skip=" . $skip . "&results=" . RECORDS_PER_PAGE;
    // Get data by calling webservice api.
    $response_array = get_publish_thisdata($url);
    // Format data as needed.
    $response = get_formatted_data($response_array);

    if (!empty($response)) {
      if (!is_object($response)) {
        $form['markup'] = array(
          '#type' => 'markup',
          '#value' => '<div class="messages error">' . $response . '</div>',
        );
      }
      else {
        $result_list = $response;
        // Save conetnt to drupal database.
        $config['datatype'] = 'feedcurated';
        save_content($result_list, $config);
      }
    }
    drupal_set_message(t('Content Saved Successfully.'));
  }
  else {
    drupal_set_message(t('Select atleast one content to save.'), 'error');
  }
  // Redirect to the page with feed id and page no.
  drupal_goto(PUBLISHTHIS_FEED_CURATECONTENT_URL . '/' . $savedid . '/' . $page_no);
}

/**
 * Implementat function to get specific feeds from publish this using feed id.
 */
function get_bundle_auto_contents() {
  $module_path = drupal_get_path('module', 'publishthis');
  drupal_add_js();

  // Prepare api url with token details.
  $url = PUBLISHTHIS_BUNDLE_API . "?token=" . TOKEN . "&results=50";
  // Get data by calling the webservice.
  $response_array = get_publish_thisdata($url);

  // Format the respone data as per required format.
  $response = get_formatted_data($response_array);

  $exist_category = FALSE;
  if (!empty($response)) {
    if (!is_object($response)) {
      $form['markup'] = array(
        '#type' => 'markup',
        '#value' => '<div class="messages error">' . $response . '</div>',
      );
    }
    else {
      $response_array = $response;
      $saved_array = $response_array->resultList;
      $selectitems[0] = '--Select published feed--';
      foreach ($saved_array as $data) {
        $exist_category = TRUE;
        $selectitems[$data->feedId] = $data->title;
      }

      natcasesort($selectitems);
    }
  }

  // Get feedid and page no from url using arg() function.
  $page = 0;
  $arguments = arg();
  $arg_len = count($arguments);
  $pg_no = $arguments[$arg_len - 1];
  $select_id = $arguments[$arg_len - 2];
  if (!is_numeric($select_id)) {
    $select_id = 0;
  }

  if (is_numeric($pg_no)) {
    $page = $pg_no;
  }

  // Prepare form to show the UI for feed display.
  drupal_set_title(t("Publishthis feed content"));
  if ($exist_category == TRUE) {
    $form['#attributes'] = array('name' => 'publishform');
    $form['savedid'] = array(
      '#type' => 'select',
      '#options' => $selectitems,
      '#value' => $select_id,
      '#prefix' => '<br/><fieldset><legend style="margin-left: 10px;">Published feeds</legend><div class="description" style="margin-left: 10px;">List of imported published feeds from Publishthis.</div><div id="edit-operation-wrapper" style="margin-left: 10px;" class="form-item">',
    );
    $form['savedid_hidden'] = array(
      '#type' => 'hidden',
      '#id' => 'edit-savedid-hidden',
    );
    $form['page_no'] = array(
      '#type' => 'hidden',
      '#value' => '0',
      '#id' => 'edit-page-no',
    );
    $form['button23'] = array(
      '#markup' => "<input type='button' class='form-submit' onclick='get_contents(0,\"GetBundleAutoContents\");' name='button' value='Get content' style='height: 27px;margin-top: 10px;' />",
      '#suffix' => '</div></fieldset>',
    );
    $module_path = drupal_get_path('module', 'publishthis');

    $image_path = "/" . $module_path . "/images/loading.gif";

    $form['submit'] = array(
      '#attributes' => array("style" => "height:27px;margin-bottom:2px;margin-top:10px;", 'title' => 'Click to save selected content'),
      '#type' => 'submit',
      '#value' => 'Save content',
      '#prefix' => '<div id="update_div" style=""><fieldset><legend style="margin-left: 10px;">Import content</legend><div class="description" id="submit_div" style="margin-left: 10px;">To import content please select checkbox against content and click Save content button.<br/>',
      '#suffix' => '</div><div id="response" style="margin-left: 10px;"></div></fieldset></div>',
    );
    $form['jssubmit'] = array(
      '#markup' => '<script>
      var loading_img = "' . $image_path . '";
      get_contents(' . $page . ',\'GetBundleAutoContents\');
      </script>',
    );
  }
  return $form;
}

/**
 * Implements hook_validate() for get_bundle_auto_contents().
 */
function get_bundle_auto_contents_validate(&$form_state, $form) {
  get_bundle_auto_contents_submit($form_state, $form);
}

/**
 * Implements hook_submit() for get_bundle_auto_contents().
 */
function get_bundle_auto_contents_submit(&$form_state, $form) {
  $content_ids = $_POST['content_ids'];
  $savedid = $_POST['savedid'];
  $page_no = $_POST['page_no'];
  extract($_POST);
  $skip = 0;
  if (!empty($content_ids) && !empty($savedid)) {
    if ($page_no > 0) {
      $skip = $page_no * RECORDS_PER_PAGE;
    }
    $url = PUBLISHTHIS_BUNDLE_AUTO_CONTENT_API . $savedid . "?token=" . TOKEN . "&skip=" . $skip . "&results=" . RECORDS_PER_PAGE;
    $response_array = get_publish_thisdata($url);
    $response = get_formatted_data($response_array);

    if (!empty($response)) {
      if (!is_object($response)) {
        $form['markup'] = array(
          '#type' => 'markup',
          '#value' => '<div class="messages error">' . $response . '</div>',
        );
      }
      else {
        $result_list = $response;
        $config['datatype'] = 'feedautomated';
        save_content($result_list, $config);
      }
    }
    drupal_set_message(t('Content Saved Successfully.'));
  }
  else {
    drupal_set_message(t('Select atleast one content to save.'), 'error');
  }
  drupal_goto(PUBLISHTHIS_FEED_AUTOCONTENT_URL . '/' . $savedid . '/' . $page_no);
}
