/**
 * @file
 * Js file for the XML API.
 */

jQuery("#savedsearches").submit(function() {
  saved_ids = jQuery("#edit-savedid").val();
  jQuery("#edit-savedid-hidden").val(saved_ids);
});

function delete_node(page) {
  saved_ids = jQuery("#edit-savedid").val();
  page_no = jQuery("#edit-page-no").val();
  var url = page + "/" + saved_ids + "/" + page_no;
  window.location.href = url;
}

function get_contents(page,func) {
  var saved_ids = "";
  var auto = '';
  if (func == 'GetBundleAutoContents') {
    auto = 'autocontent';
  }

  saved_ids = jQuery('#edit-savedid').val();

  jQuery("#edit-page-no").val(page);
  jQuery.ajax({
    type: "POST",
    timeout : 30000,
    url: "/xmlrpc.php",
    dataType:"xml",
    data: "<methodCall><methodName>" + func + "</methodName><params><value>" + saved_ids + "</value><value>" + page + "</value><value>" + auto + "</value></params></methodCall>",
    beforeSend: function() {
      jQuery('#update_div').show();
      jQuery('#response').html('<div id="loading_div"><img src=' + loading_img + ' /></div>');
    },
    success: function(show_data) {
      jQuery('#update_div').show();
      jQuery("#response").html(jQuery(show_data).find("value").text());
      jQuery(".selectall").click(function () {
        jQuery(".case").attr("checked", this.checked);
        jQuery(".selectall").attr("checked", this.checked);
      });
      jQuery(".case").click(function() {
        if (jQuery(".case").length == jQuery(".case:checked").length) {
          jQuery(".selectall").attr("checked", "checked");
        }
        else {
          jQuery(".selectall").removeAttr("checked");
        }
      });
    },
    error: function(jqXHR, textStatus) {
      var error_msg = 'Unable to retrieve content for specified criteria.';
      if (textStatus == 'timeout') {
        error_msg = 'Unable to retrieve content from publishthis server. Try after sometime.';
      }
      jQuery('#update_div').show();
      jQuery('#response').html("<div class='messages error'>" + error_msg + "</div>");
    }
  });
}

function show_hide_body() {
  var flag = $("#edit-body-exist").attr('checked');
  if (flag == true) {
    jQuery("#body_div").show();
  }
  else {
    jQuery("#body_div").hide();
  }
}

function pub_node_submit() {
  var flag = jQuery("#edit-body-exist").attr('checked');
  if (flag == false) {
    jQuery("#body-pub-content").val('');
  }
}
