<?php

/**
 * @file
 * Theme implementation to display a Publish This block.
 *
 * Available variables:
 * - $block->subject: Block title.
 * - $content: Block content.
 * - $block->module: Module that generated the block.
 * - $block->delta: An ID for the block, unique within each module.
 * - $block->region: The block region embedding the current block.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - block: The current template type, i.e., "theming hook".
 *   - block-[module]: The module generating the block. For example, the user
 *     module is responsible for handling the default user navigation block.
 *     In that case the class would be "block-user".
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $block_zebra: Outputs 'odd' and 'even' dependent on each block region.
 * - $zebra: Same output as $block_zebra but independent of any block region.
 * - $block_id: Counter dependent on each block region.
 * - $id: Same output as $block_id but independent of any block region.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 * - $block_html_id: A valid HTML ID and guaranteed unique.
 *
 * @see template_preprocess()
 * @see template_preprocess_block()
 * @see template_process()
 * @see nucleus_preprocess_block()
 * @see nucleus_process_block()
 */
?>
<?php
  if (!empty($response)):
    if (!is_object($response)):
      $news_content = '<table >';
      $news_content .= "<tr><td colspan=3><div class=\"messages error\">" . $response . "</div></td></tr></table>";
    else:
      $result_list = $response;
      $saved_array = $result_list->resultList;
      // Prepare the html result list of return webservice data.
      if (!empty($result_list->resultList)):
        $news_content = <<<STR
           <div class="publish_this_contents" >
STR;
        foreach ($saved_array as $data):
          if (!empty($data->imageUrl)):
             $img = "<img src='" . $data->imageUrl . "' width=120 height=90 />";
          else:
             $img = "&nbsp;";
          endif;
          $summary_data = strip_tags($data->summary);
          if (count($summary_data) > 100):
            $summary_data .= substr($summary_data, 0, 100) . " ...";
          endif;

          $title = $data->title;
          if (!empty($data->url)):
            $title = "<a href='" . $data->url . "' target='_blank' >" . $title . "</a>";
            $summary_data .= " <a href='" . $data->url . "' target='_blank' >Read more >></a>";
          endif;

          $news_content .= <<<STR

          <div class="publish_this_content">
            <div class="content_title">$title</div>
            <div>
               <div class="publish_this_image">$img</div>
               <div>$summary_data</div>
            </div>
          </div>
STR;
       endforeach;
       $news_content .= <<<STR
            </div>
STR;
      endif;
    endif;
  endif;
  print $news_content;
