<?php
/**
 * @file
 * Implementation function to return the formatted data or error if any.
 */

/**
 * Publishthis server host name.
 */
$server = variable_get('publish_this_server', array());
if (empty($server)) {
  $server = 'http://webapi.publishthis.com';
}
define("PUB_SERVER", $server . '/');
/*
 * Permission for create  publishthis.
 */
define("PUBLISHTHIS_URL", "admin/config/publishthis");
/**
 * Constant for Module name.
 */
define("PUBLISHTHIS_MODULE_NAME", "publishthis");
/**
 * Permission for create  publishthis.
 */
define("PUBLISHTHIS_SETTING_URL", "admin/config/publishthis/settings");

/**
 * Permission for create  publishthis admin configuration permission.
 */
define("PUBLISHTHIS_CONF_PERM", "Publishthis admin settings permission");

/**
 * Publishthis feed automated url.
 */
define("PUBLISHTHIS_FEED_AUTOCONTENT_URL", "admin/config/development/bundleautocontent");
/**
 * Publishthis feed curated url.
 */
define("PUBLISHTHIS_FEED_CURATECONTENT_URL", "admin/config/development/curatedbundlecontent");
/**
 * Publishthis delete content url.
 */
define("PUBLISHTHIS_DELETE_URL", "admin/config/content/publish/delete");
/**
 * API url for curated feed contents.
 */
define("PUBLISHTHIS_BUNDLE_CURATED_CONTENT_API", PUB_SERVER . "rest/content/curated/feed/");
/**
 * Publishthis feed curated permission.
 */
define("PUBLISHTHIS_FEED_CURATECONTENT_PERM", "Publishthis feed curated");
/**
 * Publishthis feed automated permission.
 */
define("PUBLISHTHIS_FEED_AUTOCONTENT_PERM", "Publishthis feed automated");

define("RECORDS_PER_PAGE", "20");

/**
 * Publishthis security token key.
 */
$token = variable_get('publishthis_key', array());
if (empty($token)) {
  $token = '';
}
define("TOKEN", $token);

/**
 * Implementation function to return the formatted data or error if any.
 */
function get_formatted_data($response_array) {
  if (isset($response_array->error)) {
    return "Unable to retrieve content from Publishthis server. Please try after sometime.";
  }
  elseif (!empty($response_array)) {
    $response_array = $response_array->resp;
    if ($response_array->code == '400') {
      return "Unable to retrieve content from Publishthis server. Please try after sometime.";
    }
    elseif (empty($response_array->data)) {
      return "Unable to retrieve content from Publishthis server. Please try after sometime.";
    }
    elseif ($response_array->data->totalAvailable == 0) {
      return "Content not available for specified criteria.";
    }
    else {
      return $response_array->data;
    }
  }
}

/**
 * Implement function to add javascript and css.
 */
function _publishthis_add_js_css() {
  $module_path = drupal_get_path('module', 'publishthis');
  drupal_add_js($module_path . '/js/publishform.js');
  drupal_add_css($module_path . '/images/form_theme.css');
  drupal_add_css(drupal_get_path('module', 'publishthis') . '/css/custom.css', array('group' => CSS_DEFAULT, 'every_page' => TRUE));
}

/**
 * RPC function written to handle the ajax request.
 */
function publishthis_xmlrpc() {
  return array(
    'GetSavedContents' => '_get_savedsearch_contents',
    'GetBundleContents' => '_get_bundle_contents',
    'GetBundleAutoContents' => '_get_bundle_contents',
  );
}

/**
 * API url for feeds.
 */
define("PUBLISHTHIS_BUNDLE_API", PUB_SERVER . "rest/feeds/");

/**
 * API url for auto feed contents.
 */
define("PUBLISHTHIS_BUNDLE_AUTO_CONTENT_API", PUB_SERVER . "rest/content/automated/feed/");

/**
 * Implement function to save content to node and questexpublishthis table.
 */
function save_content($result_list, $config = array(), $save_content_data = '') {
  $content_ids = $_POST['content_ids'];
  $savedid = $_POST['savedid'];
  $templid = 0;
  extract($_POST);
  // If save_content_data contain array data means take saved_content from.
  // Save_content_data otherwise prepare array based on result_list value.
  if (empty($save_content_data)) {
    if (!empty($result_list->resultList)) {
      $saved_array = $result_list->resultList;
      foreach ($saved_array as $data) {
        // Prepare savedcontent array.
        $saved_contents[$data->docId]['title'] = $data->title;
        $saved_contents[$data->docId]['teaser'] = $data->summary;
        $saved_contents[$data->docId]['source_url'] = $data->url;
        $saved_contents[$data->docId]['img'] = $data->imageUrl;
        $saved_contents[$data->docId]['pubdate'] = $data->publishDate;
        $saved_contents[$data->docId]['contentType'] = $data->contentType;
        if (isset($data->curateUpdateDate)) {
          $saved_contents[$data->docId]['curateUpdateDate'] = $data->curateUpdateDate;
        }
        if (isset($data->feed_id)) {
          $saved_contents[$data->docId]['feed_id'] = $data->feed_id;
        }
        if (isset($data->annotations)) {
          $saved_contents[$data->docId]['annotations'] = $data->annotations[0]->annotation;
        }
      }
    }
  }
  else {
    $saved_contents = $save_content_data;
  }
  foreach ($content_ids as $cid) {
    $node = new stdClass();
    $node->type = 'publishthis';
    $node->uid = 1;
    $node->status = 1;

    if (!empty($saved_contents[$cid])) {
      // Assign value to node.
      $node->title = $node->page_title = $saved_contents[$cid]['title'];
      $teaser = preg_replace("/&#?[a-z0-9]+;/i", "", $saved_contents[$cid]['teaser']);
      $node->body[LANGUAGE_NONE][0]['value'] = $teaser;
      $node->body[LANGUAGE_NONE][0]['format'] = 'full_html';
      $node->body[LANGUAGE_NONE][0]['summary'] = $teaser;
      $node->language = LANGUAGE_NONE;
      if (!empty($saved_contents[$cid]['annotations'])) {
        $node->publishthis_annotations[LANGUAGE_NONE][0]['value'] = $saved_contents[$cid]['annotations'];
      }
      $node->publishthis_img_url[LANGUAGE_NONE][0]['value'] = $saved_contents[$cid]['img'];
      $node->publishthis_source_url[LANGUAGE_NONE][0]['value'] = $saved_contents[$cid]['source_url'];
      $node->publishthis_publish_date[LANGUAGE_NONE][0]['value'] = $saved_contents[$cid]['pubdate'];
      if (!empty($saved_contents[$cid]['curateUpdateDate'])) {
        $node->publishthis_update_date[LANGUAGE_NONE][0]['value'] = $saved_contents[$cid]['curateUpdateDate'];
      }
      $node->publishthis_sub_type[LANGUAGE_NONE][0]['value'] = $saved_contents[$cid]['contentType'];
      $node->publishthis_category_type[LANGUAGE_NONE][0]['value'] = $config['datatype'];
      $node->publishthis_content_id[LANGUAGE_NONE][0]['value'] = $cid;
      if (!empty($templid)) {
        $node->publishthis_template_id[LANGUAGE_NONE][0]['value'] = $templid;
      }
      $node->publishthis_category_id[LANGUAGE_NONE][0]['value'] = $savedid;
      $node->publishthis_import_date[LANGUAGE_NONE][0]['value'] = date("Y-m-d H:i:s");

      node_save($node);
      $nid = $node->nid;
      db_update("update {node} set status = 1 where nid = :nid", array(':nid' => $nid));
    }
  }
}

/**
 * Implement function to save content to node and publishthis table.
 */
function update_content($result_list, $config = array(), $saved_contents = '') {
  $save_content_data = $_POST['save_content_data'];
  $savedid = $_POST['savedid'];
  $updated_content_ids = $_POST['updated_content_ids'];
  extract($_POST);
  // If save_content_data contain array data means take saved_content from.
  // Save_content_data otherwise prepare array based on result_list value.
  if (empty($save_content_data)) {
    if (!empty($result_list->resultList)) {
      $saved_array = $result_list->resultList;
      foreach ($saved_array as $data) {
        // Prepare savedcontent array.
        $saved_contents[$data->docId]['title'] = $data->title;
        $saved_contents[$data->docId]['teaser'] = $data->summary;
        $saved_contents[$data->docId]['source_url'] = $data->url;
        $saved_contents[$data->docId]['img'] = $data->imageUrl;
        $saved_contents[$data->docId]['pubdate'] = $data->publishDate;
        $saved_contents[$data->docId]['contentType'] = $data->contentType;
        if ($data->curateUpdateDate) {
          $saved_contents[$data->docId]['curateUpdateDate'] = $data->curateUpdateDate;
        }
        if ($data->annotations) {
          $saved_contents[$data->docId]['annotations'] = $data->annotations[0]->annotation;
        }
      }
    }
  }
  else {
    $saved_contents = $save_content_data;
  }
  foreach ($updated_content_ids as $cid) {
    $result = db_query("SELECT entity_id FROM {field_data_publishthis_content_id} WHERE publishthis_content_id_value = :content_id_value", array(':content_id_value' => $cid));
    foreach ($result as $record) {
      $node_id = $record->entity_id;
    }
    $node = node_load($node_id);
    $node = new stdClass();
    $node->type = 'publishthis';
    $node->uid = 1;
    $node->status = 1;

    if (!empty($saved_contents[$cid])) {
      // Assign value to node.
      $node->title = $node->page_title = $saved_contents[$cid]['title'];
      $teaser = preg_replace("/&#?[a-z0-9]+;/i", "", $saved_contents[$cid]['teaser']);

      $node->body[LANGUAGE_NONE][0]['value'] = $teaser;
      $node->body[LANGUAGE_NONE][0]['format'] = 'full_html';
      $node->body[LANGUAGE_NONE][0]['summary'] = $teaser;
      $node->language = LANGUAGE_NONE;
      $node->publishthis_annotations[LANGUAGE_NONE][0]['value'] = $saved_contents[$cid]['annotations'];
      $node->publishthis_img_url[LANGUAGE_NONE][0]['value'] = $saved_contents[$cid]['img'];
      $node->publishthis_source_url[LANGUAGE_NONE][0]['value'] = $saved_contents[$cid]['source_url'];
      $node->publishthis_publish_date[LANGUAGE_NONE][0]['value'] = $saved_contents[$cid]['pubdate'];
      $node->publishthis_update_date[LANGUAGE_NONE][0]['value'] = $saved_contents[$cid]['curateUpdateDate'];
      $node->publishthis_sub_type[LANGUAGE_NONE][0]['value'] = $saved_contents[$cid]['contentType'];
      $node->publishthis_category_type[LANGUAGE_NONE][0]['value'] = $config['datatype'];
      $node->publishthis_content_id[LANGUAGE_NONE][0]['value'] = $cid;
      $node->publishthis_template_id[LANGUAGE_NONE][0]['value'] = $node->templid;
      $node->publishthis_category_id[LANGUAGE_NONE][0]['value'] = $savedid;
      $node->publishthis_import_date[LANGUAGE_NONE][0]['value'] = date("Y-m-d H:i:s");

      node_save($node);

      $nid = $node->nid;
      db_update("update {node} set status = 1 where nid = %d", $nid);
    }
  }
}

/**
 * Implement function to return the existing pubid available @ drupal database.
 */
function get_existing_pubids() {
  $existing_pubids = array();
  $pub_obj = db_query("SELECT publishthis_content_id_value FROM {field_data_publishthis_content_id}");
  foreach ($pub_obj as $row) {
    $existing_pubids[] = $row->publishthis_content_id_value;
  }
  return $existing_pubids;
}

/**
 * Implement function to return pagination output.
 */
function get_publishthis_pagination($config = array()) {

  $page = $config['page'];
  $method = $config['method'];
  $next_flag = $config['next_flag'];
  $page_output = '<ul class = "pager" >';

  if ($page > 0) {
    $page_output .= '<li class="pager-previous"><a href="javascript:void(0);" onclick="get_contents(' . ($page - 1) . ',\'' . $method . '\');" title="Go to previous page" class="active">&laquo; previous</a></li>';
    $page_output .= '<li class="pager-current">' . ($page + 1) . '</li>';
  }
  elseif ($page == 0) {
    $page_output .= '<li class="pager-current">' . ($page + 1) . '</li>';
  }

  if ($next_flag) {
    $page_output .= '<li class="pager-next"><a href="javascript:void(0);" onclick="get_contents(' . ($page + 1) . ',\'' . $method . '\');" title="Go to next page" class="active">next &raquo;</a></li>';
  }

  $page_output .= '</ul>';
  return $page_output;
}
